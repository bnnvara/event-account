<?php

namespace Tests\BNNVARA\Account\Domain\Command;

use BNNVARA\Account\Domain\Command\SynchronizeFinancialCommand;
use BNNVARA\Account\Domain\ValueObject\FinancialId;
use PHPUnit\Framework\TestCase;

class SynchronizeFinancialCommandTest extends TestCase
{
    /** @test */
    public function anAccountMergeCommandCanBeCreated(): void
    {
        $financialId = new FinancialId('12345678-1234-1234-1234-123456789012');
        $command = new SynchronizeFinancialCommand($financialId);

        $this->assertInstanceOf(SynchronizeFinancialCommand::class, $command);
        $this->assertInstanceOf(FinancialId::class, $command->getData());
    }
}
