<?php

namespace Tests\BNNVARA\Account\Domain\Command;

use BNNVARA\Account\Domain\Command\AccountMergeCommand;
use BNNVARA\Account\Domain\ValueObject\AccountId;
use PHPUnit\Framework\TestCase;

class AccountMergeCommandTest extends TestCase
{
    /** @test */
    public function anAccountMergeCommandCanBeCreated(): void
    {
        $accountId = new AccountId('12345678-1234-1234-1234-123456789012');
        $command = new AccountMergeCommand($accountId);

        $this->assertInstanceOf(AccountMergeCommand::class, $command);
        $this->assertInstanceOf(AccountId::class, $command->getData());
    }
}