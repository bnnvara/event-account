<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Account\Domain\Event;

use BNNVARA\Account\Domain\Event\AccountCreatedEvent;
use BNNVARA\Account\Domain\ValueObject\Account;
use PHPUnit\Framework\TestCase;

class AccountCreatedEventTest extends TestCase
{
    /** @test */
    public function anAccountCreatedEventCanBeCreated(): void
    {
        $account = $this->getMockBuilder(
            Account::class
        )->disableOriginalConstructor()->getMock();

        $event = new AccountCreatedEvent($account);

        $this->assertInstanceOf(AccountCreatedEvent::class, $event);
        $this->assertInstanceOf(Account::class, $event->getData());
    }
}
