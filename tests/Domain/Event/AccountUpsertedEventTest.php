<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Account\Domain\Event;

use BNNVARA\Account\Domain\Event\AccountUpsertedEvent;
use BNNVARA\Account\Domain\ValueObject\Account;
use PHPUnit\Framework\TestCase;

class AccountUpsertedEventTest extends TestCase
{
    /** @test */
    public function anAccountUpsertedEventCanBeCreated(): void
    {
        $account = $this->getMockBuilder(
            Account::class
        )->disableOriginalConstructor()->getMock();

        $event = new AccountUpsertedEvent($account);

        $this->assertInstanceOf(AccountUpsertedEvent::class, $event);
        $this->assertInstanceOf(Account::class, $event->getData());
    }
}
