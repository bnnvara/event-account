<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Account\Domain\Event;

use BNNVARA\Account\Domain\Event\AccountUpdatedEvent;
use BNNVARA\Account\Domain\ValueObject\Account;
use PHPUnit\Framework\TestCase;

class AccountUpdatedEventTest extends TestCase
{
    /** @test */
    public function anAccountUpdatedEventCanBeCreated(): void
    {
        $account = $this->getAccount();
        $event = new AccountUpdatedEvent($account);

        $this->assertInstanceOf(AccountUpdatedEvent::class, $event);
        $this->assertInstanceOf(Account::class, $event->getData());
    }

    private function getAccount(): Account
    {
        $account = $this->getMockBuilder(
            Account::class
        )->disableOriginalConstructor()->getMock();

        /** @var Account $account */
        return $account;
    }
}
