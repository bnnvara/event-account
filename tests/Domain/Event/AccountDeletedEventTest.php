<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Account\Domain\Event;

use BNNVARA\Account\Domain\Event\AccountDeletedEvent;
use BNNVARA\Account\Domain\ValueObject\AccountId;
use PHPUnit\Framework\TestCase;

class AccountDeletedEventTest extends TestCase
{
    /** @test */
    public function anAccountDeletedEventCanBeCreated(): void
    {
        $accountId = $this->getAccountId();
        $event = new AccountDeletedEvent($accountId);

        $this->assertInstanceOf(AccountDeletedEvent::class, $event);
        $this->assertInstanceOf(AccountId::class, $event->getData());
    }

    private function getAccountId(): AccountId
    {
        $accountId = $this->getMockBuilder(AccountId::class)->disableOriginalConstructor()->getMock();

        /** @var AccountId $accountId */
        return $accountId;
    }

}
