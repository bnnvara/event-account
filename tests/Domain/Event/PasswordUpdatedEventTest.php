<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Account\Domain\Event;

use BNNVARA\Account\Domain\Event\PasswordUpdatedEvent;
use PHPUnit\Framework\TestCase;

class PasswordUpdatedEventTest extends TestCase
{
    /** @test */
    public function itCanBeCreated(): void
    {
        $event = new PasswordUpdatedEvent('account-123', 'secret1!');
        $this->assertSame('account-123', $event->getAccountId());
        $this->assertSame('secret1!', $event->getNewPassword());
    }
}
