<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Account\Domain\ValueObject;

use BNNVARA\Account\Domain\Exception\InvalidConsentTypeException;
use BNNVARA\Account\Domain\ValueObject\Consent;
use PHPUnit\Framework\TestCase;

class ConsentTest extends TestCase
{
    /** @test
     * @dataProvider getConsents
     */
    public function aConsentCanBeCreated(string $type, bool $isGranted, string $lastUpdated): void
    {
        $consent = new Consent($type, $isGranted, $lastUpdated);

        $this->assertInstanceOf(Consent::class, $consent);
        $this->assertEquals($type, $consent->getType());
        $this->assertEquals($isGranted, $consent->isGranted());
        $this->assertEquals($lastUpdated, $consent->getLastUpdated());
    }

    /** @test */
    public function anExceptionIsThrownWhenAnInvalidTypeIsProvided(): void
    {
        $this->expectException(InvalidConsentTypeException::class);

        new Consent('marketingggggg', true, '2017-10-17T18:02:56.391Z');
    }

    public function getConsents(): array
    {
        return [
            [
                Consent::ALLOWED_TYPE_MARKETING,
                true,
                '2017-10-17T18:02:56.391Z'
            ],
            [
                'tracking',
                false,
                '2017-10-17T18:02:56.391Z'
            ],
        ];
    }
}
