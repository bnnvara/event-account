<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Account\Domain\ValueObject;

use BNNVARA\Account\Domain\ValueObject\Address;
use PHPUnit\Framework\TestCase;

class AddressTest extends TestCase
{
    /** @test
     * @dataProvider getAddresses
     */
    public function anAddressCanBeCreated(
        string $streetName,
        string $houseNumber,
        string $zipCode,
        string $country,
        string $city,
        string $houseNumberAddition = ''
    ): void {
        $address = new Address(
            $streetName,
            $houseNumber,
            $zipCode,
            $country,
            $city,
            $houseNumberAddition
        );

        $this->assertInstanceOf(Address::class, $address);
        $this->assertEquals($streetName, $address->getStreetName());
        $this->assertEquals($houseNumber, $address->getHouseNumber());
        $this->assertEquals($zipCode, $address->getZipCode());
        $this->assertEquals($country, $address->getCountry());
        $this->assertEquals($city, $address->getCity());
        $this->assertEquals($houseNumberAddition, $address->getHouseNumberAddition());
    }

    public function getAddresses(): array
    {
        return [
            [
                'straatnaam',
                '313',
                '0000 ZZ',
                'NL',
                'Hilversum',
                'a'
            ],
            [
                'straatnaam',
                '313',
                '0000 ZZ',
                'NL',
                'Hilversum'
            ]
        ];
    }
}
