<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Account\Domain\ValueObject;

use BNNVARA\Account\Domain\Collection\ConsentCollection;
use BNNVARA\Account\Domain\ValueObject\Account;
use BNNVARA\Account\Domain\ValueObject\AccountId;
use BNNVARA\Account\Domain\ValueObject\Address;
use BNNVARA\Account\Domain\ValueObject\Consent;
use BNNVARA\Account\Domain\ValueObject\DateOfBirth;
use BNNVARA\Account\Domain\ValueObject\LegalAcceptances;
use BNNVARA\Account\Domain\ValueObject\Password;
use PHPUnit\Framework\TestCase;

class AccountTest extends TestCase
{
    /** @test */
    public function anAccountCanBeCreated(): void
    {
        $consentCollection = new ConsentCollection();
        $consent1 = new Consent('tracking', true, '2011-10-17T18:02:56.392Z');
        $consent2 = new Consent('marketing', false, '2012-10-17T18:02:56.392Z');
        $consentCollection->add($consent1);
        $consentCollection->add($consent2);

        $account = new Account(
            new AccountId('12345678-1234-1234-12345678'),
            '2010-10-17T18:02:56.392Z',
            'externalId',
            'Dit is mijn vette biografie! Dit is mijn vette biografie! Dit is mijn vette biografie! Dit is mijn vette biografie! Dit is mijn vette biografie!',
            $consentCollection,
            'contactNumber',
            new DateOfBirth(1989, 12, 11),
            'displayName',
            'emailaddress@example.com',
            '2013-10-17T18:02:56.392Z',
            'familyName',
            'firstContact',
            'gender',
            'givenName',
            true,
            false,
            false,
            true,
            true,
            new LegalAcceptances(true, '2014-10-17T18:02:56.392Z'),
            'middleName',
            new Password('myhash', 'mysalt'),
            new Address('streetname', '123', '0000 ZZ', 'NL', 'City'),
            'http://thisisanurl.example.com',
            'registrationSource',
            'relationNumber',
            'userName'
        );

        $this->assertInstanceOf(Account::class, $account);
        $this->assertInstanceOf(AccountId::class, $account->getAccountId());
        $this->assertEquals('2010-10-17T18:02:56.392Z', $account->getCreated());
        $this->assertEquals('externalId', $account->getExternalId());
        $this->assertEquals(
            'Dit is mijn vette biografie! Dit is mijn vette biografie! Dit is mijn vette biografie! Dit is mijn vette biografie! Dit is mijn vette biografie!',
            $account->getBio()
        );
        $this->assertInstanceOf(ConsentCollection::class, $account->getConsentCollection());
        $this->assertEquals('contactNumber', $account->getContactNumber());
        $this->assertInstanceOf(DateOfBirth::class, $account->getDateOfBirth());
        $this->assertEquals('displayName', $account->getDisplayName());
        $this->assertEquals('emailaddress@example.com', $account->getEmail());
        $this->assertEquals('2013-10-17T18:02:56.392Z', $account->getEmailVerified());
        $this->assertEquals('familyName', $account->getFamilyName());
        $this->assertEquals('firstContact', $account->getFirstContact());
        $this->assertEquals('gender', $account->getGender());
        $this->assertEquals('givenName', $account->getGivenName());
        $this->assertTrue($account->isActive());
        $this->assertFalse($account->isLite());
        $this->assertFalse($account->isLockedOut());
        $this->assertTrue($account->isRegistered());
        $this->assertTrue($account->isVerified());
        $this->assertInstanceOf(LegalAcceptances::class, $account->getLegalAcceptances());
        $this->assertEquals('middleName', $account->getMiddleName());
        $this->assertInstanceOf(Password::class, $account->getPassword());
        $this->assertInstanceOf(Address::class, $account->getPrimaryAddress());
        $this->assertEquals('http://thisisanurl.example.com', $account->getAvatarUrl());
        $this->assertEquals('registrationSource', $account->getRegistrationSource());
        $this->assertEquals('relationNumber', $account->getRelationNumber());
        $this->assertEquals('userName', $account->getUsername());
    }

    /** @test */
    public function objectsCanBeNullable(): void
    {
        $consentCollection = new ConsentCollection();
        $consent1 = new Consent('tracking', true, '2011-10-17T18:02:56.392Z');
        $consent2 = new Consent('marketing', false, '2012-10-17T18:02:56.392Z');
        $consentCollection->add($consent1);
        $consentCollection->add($consent2);

        $account = new Account(
            new AccountId('12345678-1234-1234-12345678'),
            '2010-10-17T18:02:56.392Z',
            'externalId',
            'Dit is mijn vette biografie! Dit is mijn vette biografie! Dit is mijn vette biografie! Dit is mijn vette biografie! Dit is mijn vette biografie!',
            $consentCollection,
            'contactNumber',
            null,
            'displayName',
            'emailaddress@example.com',
            '2013-10-17T18:02:56.392Z',
            'familyName',
            'firstContact',
            'gender',
            'givenName',
            true,
            false,
            false,
            true,
            true,
            new LegalAcceptances(true, '2014-10-17T18:02:56.392Z'),
            'middleName',
            null,
            null,
            'http://thisisanurl.example.com',
            'registrationSource',
            'relationNumber',
            'userName'
        );

        $this->assertNull($account->getDateOfBirth());
        $this->assertNull($account->getPassword());
        $this->assertNull($account->getPrimaryAddress());
    }
}
