<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Account\Domain\ValueObject;

use BNNVARA\Account\Domain\ValueObject\DateOfBirth;
use PHPUnit\Framework\TestCase;

class DateOfBirthTest extends TestCase
{
    /** @test */
    public function aDateOfBirthCanBeCreated(): void
    {
        $dateOfBirth = new DateOfBirth(1989, 12, 11);

        $this->assertInstanceOf(DateOfBirth::class, $dateOfBirth);
        $this->assertEquals(1989, $dateOfBirth->getBirthYear());
        $this->assertEquals(12, $dateOfBirth->getBirthMonth());
        $this->assertEquals(11, $dateOfBirth->getBirthDay());
    }
}
