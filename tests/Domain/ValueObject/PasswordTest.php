<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Account\Domain\ValueObject;

use BNNVARA\Account\Domain\ValueObject\Password;
use PHPUnit\Framework\TestCase;

class PasswordTest extends TestCase
{
    /** @test */
    public function aPasswordCanBeCreated(): void
    {
        $password = new Password('$2a$10$djs7WyVhY/ogkXsfsAzMyu.DciaPsdlCT4Yla7xt8r3Dh46HTn0pK', 'password-bcrypt');

        $this->assertInstanceOf(Password::class, $password);
        $this->assertEquals('$2a$10$djs7WyVhY/ogkXsfsAzMyu.DciaPsdlCT4Yla7xt8r3Dh46HTn0pK', $password->getValue());
        $this->assertEquals('password-bcrypt', $password->getType());
    }
}
