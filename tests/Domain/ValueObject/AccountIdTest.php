<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Account\Domain\ValueObject;

use BNNVARA\Account\Domain\ValueObject\AccountId;
use PHPUnit\Framework\TestCase;

class AccountIdTest extends TestCase
{
    /** @test */
    public function anAccountIdCanBeCreated(): void
    {
        $accountId = new AccountId('12345678-1234-1224-12345678');

        $this->assertInstanceOf(AccountId::class, $accountId);
        $this->assertEquals('12345678-1234-1224-12345678', (string)$accountId);
    }
}
