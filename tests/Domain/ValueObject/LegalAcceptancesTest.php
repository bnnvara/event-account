<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Account\Domain\ValueObject;

use BNNVARA\Account\Domain\ValueObject\LegalAcceptances;
use PHPUnit\Framework\TestCase;

class LegalAcceptancesTest extends TestCase
{
    /** @test
     * @dataProvider getAcceptances
     */
    public function aLegalAcceptancesCanBeCreated($hasAccepted, $dateAccepted = ''): void
    {
        $legalAcceptances = new LegalAcceptances($hasAccepted, $dateAccepted);

        $this->assertInstanceOf(LegalAcceptances::class, $legalAcceptances);
        $this->assertEquals($hasAccepted, $legalAcceptances->hasAcceptedTerms());
        $this->assertEquals($dateAccepted, $legalAcceptances->getDateAccepted());
    }

    public function getAcceptances(): array
    {
        return [
            [
                true,
                '2017-10-17T18:02:56.392Z'
            ],
            [
                false
            ]
        ];
    }

}
