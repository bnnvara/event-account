<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Account\Domain\Collection;

use BNNVARA\Account\Domain\Collection\ConsentCollection;
use BNNVARA\Account\Domain\ValueObject\Consent;
use PHPUnit\Framework\TestCase;

class ConsentCollectionTest extends TestCase
{
    /** @test */
    public function aConsentCollectionCanBeCreated(): void
    {
        $consentCollection = $this->getCollection();

        $this->assertInstanceOf(ConsentCollection::class, $consentCollection);
        $this->assertCount(2, $consentCollection);
        $this->assertContainsOnly(Consent::class, $consentCollection);

        $this->assertEquals('tracking', $consentCollection[0]->getType());
        $this->assertEquals(true, $consentCollection[0]->isGranted());
        $this->assertEquals('2017-10-17T18:02:56.391Z', $consentCollection[0]->getLastUpdated());
        $this->assertEquals('marketing', $consentCollection[1]->getType());
        $this->assertEquals(false, $consentCollection[1]->isGranted());
        $this->assertEquals('2017-10-17T18:02:56.392Z', $consentCollection[1]->getLastUpdated());
    }

    /** @test */
    public function itReturnsTheCorrectAmountOfElements(): void
    {
        $consentCollection = $this->getCollection();

        $this->assertCount(2, $consentCollection);
        $this->assertEquals(2, $consentCollection->count());
    }

    /** @test */
    public function itGetsTheNextRecordAndResetsThePointer(): void
    {
        $consentCollection = $this->getCollection();

        $consentCollection->next();
        $this->assertEquals(1, $consentCollection->key());

        $consentCollection->rewind();
        $this->assertEquals(0, $consentCollection->key());
    }

    /** @test */
    public function itCanRetrieveTheCurrentKey(): void
    {
        $consentCollection = $this->getCollection();
        $consent2 = new Consent('marketing', false, '2017-10-17T18:02:56.392Z');

        $this->assertEquals(0, $consentCollection->key());

        $consentCollection->next();

        $this->assertEquals($consent2, $consentCollection->current());
    }

    /** @test */
    public function checksIfAKeyIsValid(): void
    {
        $consentCollection = $this->getCollection();

        $this->assertTrue($consentCollection->offsetExists(1));
        $this->assertFalse($consentCollection->offsetExists(3));
    }

    /** @test */
    public function itReturnsNullOrCorrectObject(): void
    {
        $consentCollection = $this->getCollection();

        $this->assertNull($consentCollection->offsetGet(4));
        $this->assertInstanceOf(Consent::class, $consentCollection->offsetGet(1));
    }

    /** @test */
    public function itUnsetsAnItemInTheCollection(): void
    {
        $consentCollection = $this->getCollection();
        $consent = new Consent('marketing', false, '2017-10-17T18:02:56.392Z');

        $this->assertCount(2, $consentCollection);

        $consentCollection->offsetUnset(0);

        $this->assertCount(1, $consentCollection);
        $this->assertNull($consentCollection->offsetGet(0));

        $this->assertEquals($consent, $consentCollection->offsetGet(1));
    }

    /** @test */
    public function itSetsAnItemOnASpecificOffset(): void
    {
        $consentCollection = $this->getCollection();
        $consent = new Consent('tracking', false, '2016-10-17T18:02:56.392Z');

        $this->assertCount(2, $consentCollection);
        $consentCollection->offsetSet(2, $consent);
        $this->assertCount(3, $consentCollection);

        $consentCollection->offsetSet(1, $consent);
        $this->assertEquals($consent, $consentCollection->offsetGet(1));
    }

    /** @test */
    public function itReturnsAJsonString(): void
    {
        $consentCollection = $this->getCollection();

        $this->assertJson($consentCollection->serialize());
        $this->assertJsonStringEqualsJsonString('
            [   
                "{\"type\":\"tracking\",\"isGranted\":true,\"lastUpdated\":\"2017-10-17T18:02:56.391Z\"}",
                "{\"type\":\"marketing\",\"isGranted\":false,\"lastUpdated\":\"2017-10-17T18:02:56.392Z\"}"
            ] ',
            $consentCollection->serialize());
    }

    private function getCollection(): ConsentCollection
    {
        $consentCollection = new ConsentCollection();
        $consent1 = new Consent('tracking', true, '2017-10-17T18:02:56.391Z');
        $consent2 = new Consent('marketing', false, '2017-10-17T18:02:56.392Z');
        $consentCollection->add($consent1);
        $consentCollection->add($consent2);

        return $consentCollection;
    }
}
