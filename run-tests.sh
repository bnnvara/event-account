#!/bin/sh

curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
composer install

vendor/bin/phpunit --stop-on-failure --fail-on-empty-test-suite
