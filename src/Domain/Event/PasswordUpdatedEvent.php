<?php

declare(strict_types=1);

namespace BNNVARA\Account\Domain\Event;

class PasswordUpdatedEvent
{
    private string $accountId;
    private string $newPassword;

    public function __construct(string $accountId, string $newPassword)
    {
        $this->accountId = $accountId;
        $this->newPassword = $newPassword;
    }

    public function getAccountId(): string
    {
        return $this->accountId;
    }

    public function getNewPassword(): string
    {
        return $this->newPassword;
    }
}
