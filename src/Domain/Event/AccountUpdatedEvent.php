<?php

declare(strict_types=1);

namespace BNNVARA\Account\Domain\Event;

use BNNVARA\Account\Domain\ValueObject\Account;

class AccountUpdatedEvent implements AccountUpsertedEventInterface
{
    private Account $data;

    public function __construct(Account $data)
    {
        $this->data = $data;
    }

    public function getData(): Account
    {
        return $this->data;
    }
}
