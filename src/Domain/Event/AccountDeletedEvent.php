<?php

declare(strict_types=1);

namespace BNNVARA\Account\Domain\Event;

use BNNVARA\Account\Domain\ValueObject\AccountId;

class AccountDeletedEvent
{
    private AccountId $data;

    public function __construct(AccountId $data)
    {
        $this->data = $data;
    }

    public function getData(): AccountId
    {
        return $this->data;
    }
}
