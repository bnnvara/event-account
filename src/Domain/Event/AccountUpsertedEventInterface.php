<?php

declare(strict_types=1);

namespace BNNVARA\Account\Domain\Event;

use BNNVARA\Account\Domain\ValueObject\Account;

interface AccountUpsertedEventInterface
{
    public function getData(): Account;
}
