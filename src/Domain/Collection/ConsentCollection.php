<?php

declare(strict_types=1);

namespace BNNVARA\Account\Domain\Collection;

use ArrayAccess;
use BNNVARA\Account\Domain\ValueObject\Consent;
use Countable;
use Iterator;

class ConsentCollection implements Iterator, Countable, ArrayAccess
{
    private array $consents = [];
    private int $position = 0;

    public function add(Consent $consent): void
    {
        array_push($this->consents, $consent);
    }

    public function getConsents(): array
    {
        return $this->consents;
    }

    public function count(): int
    {
        return count($this->consents);
    }

    public function rewind(): void
    {
        $this->position = 0;
    }

    public function current(): Consent
    {
        return $this->consents[$this->position];
    }

    public function key(): int
    {
        return $this->position;
    }

    public function next(): void
    {
        ++$this->position;
    }

    public function valid(): bool
    {
        return isset($this->consents[$this->position]);
    }

    public function serialize(): string
    {
        return json_encode($this->consents);
    }

    public function unserialize($serialized): void
    {
        $this->consents = unserialize($serialized);
    }

    public function offsetExists($offset): bool
    {
        return isset($this->consents[$offset]);
    }

    public function offsetGet($offset): ?Consent
    {
        return $this->consents[$offset] ?? null;
    }

    public function offsetSet($offset, $value): void
    {
        if (is_null($offset)) {
            $this->consents[] = $value;
        } else {
            $this->consents[$offset] = $value;
        }
    }

    public function offsetUnset($offset): void
    {
        unset($this->consents[$offset]);
    }
}
