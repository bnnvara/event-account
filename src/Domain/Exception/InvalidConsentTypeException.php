<?php

declare(strict_types=1);

namespace BNNVARA\Account\Domain\Exception;

use Exception;

class InvalidConsentTypeException extends Exception
{
}
