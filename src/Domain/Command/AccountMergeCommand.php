<?php

namespace BNNVARA\Account\Domain\Command;

use BNNVARA\Account\Domain\ValueObject\AccountId;

class AccountMergeCommand
{
    private AccountId $data;

    public function __construct(AccountId $data)
    {
        $this->data = $data;
    }

    public function getData(): AccountId
    {
        return $this->data;
    }
}