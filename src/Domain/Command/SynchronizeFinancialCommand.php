<?php

namespace BNNVARA\Account\Domain\Command;

use BNNVARA\Account\Domain\ValueObject\FinancialId;

class SynchronizeFinancialCommand
{
    private FinancialId $data;

    public function __construct(FinancialId $data)
    {
        $this->data = $data;
    }

    public function getData(): FinancialId
    {
        return $this->data;
    }
}
