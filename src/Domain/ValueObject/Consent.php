<?php

declare(strict_types=1);

namespace BNNVARA\Account\Domain\ValueObject;

use BNNVARA\Account\Domain\Exception\InvalidConsentTypeException;

class Consent implements \JsonSerializable
{
    public const ALLOWED_TYPE_MARKETING = 'marketing';
    public const ALLOWED_TYPE_TRACKING = 'tracking';

    private string $type;
    private bool $isGranted;
    private string $lastUpdated;

    /** @throws InvalidConsentTypeException */
    public function __construct(string $type, bool $isGranted, string $lastUpdated)
    {
        if ($type === self::ALLOWED_TYPE_MARKETING || $type === self::ALLOWED_TYPE_TRACKING) {
            $this->type = $type;
            $this->isGranted = $isGranted;
            $this->lastUpdated = $lastUpdated;
        } else {
            throw new InvalidConsentTypeException();
        }
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function isGranted(): bool
    {
        return $this->isGranted;
    }

    public function getLastUpdated(): string
    {
        return $this->lastUpdated;
    }

    public function jsonSerialize(): string
    {
       return json_encode(
           [
               'type' => $this->getType(),
               'isGranted' => $this->isGranted,
               'lastUpdated' => $this->getLastUpdated()
           ]
       );
    }

}
