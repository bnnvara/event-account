<?php

declare(strict_types=1);

namespace BNNVARA\Account\Domain\ValueObject;

class FinancialId
{
    /** @var string */
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function __toString(): string
    {
        return $this->id;
    }
}
