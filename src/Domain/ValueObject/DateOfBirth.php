<?php

declare(strict_types=1);

namespace BNNVARA\Account\Domain\ValueObject;

class DateOfBirth
{
    private int $birthYear;
    private int $birthMonth;
    private int $birthDay;

    public function __construct(int $birthYear, int $birthMonth, int $birthDay)
    {
        $this->birthYear = $birthYear;
        $this->birthMonth = $birthMonth;
        $this->birthDay = $birthDay;
    }

    public function getBirthYear(): int
    {
        return $this->birthYear;
    }

    public function getBirthMonth(): int
    {
        return $this->birthMonth;
    }

    public function getBirthDay(): int
    {
        return $this->birthDay;
    }
}
