<?php

declare(strict_types=1);

namespace BNNVARA\Account\Domain\ValueObject;

use BNNVARA\Account\Domain\Collection\ConsentCollection;

class Account
{
    private AccountId $accountId;
    private string $created;
    private string $externalId;
    private string $bio;
    private ConsentCollection $consentCollection;
    private string $contactNumber;
    private ?DateOfBirth $dateOfBirth;
    private string $displayName;
    private string $email;
    private string $emailVerified;
    private string $familyName;
    private string $firstContact;
    private string $gender;
    private string $givenName;
    private bool $isActive;
    private bool $isLite;
    private bool $isLockedOut;
    private bool $isRegistered;
    private bool $isVerified;
    private LegalAcceptances $legalAcceptances;
    private string $middleName;
    private ?Password $password;
    private ?Address $primaryAddress;
    private string $avatarUrl;
    private string $registrationSource;
    private string $relationNumber;
    private string $username;

    public function __construct(
        AccountId $accountId,
        string $created,
        string $externalId,
        string $bio,
        ConsentCollection $consentCollection,
        string $contactNumber,
        ?DateOfBirth $dateOfBirth,
        string $displayName,
        string $email,
        string $emailVerified,
        string $familyName,
        string $firstContact,
        string $gender,
        string $givenName,
        bool $isActive,
        bool $isLite,
        bool $isLockedOut,
        bool $isRegistered,
        bool $isVerified,
        LegalAcceptances $legalAcceptances,
        string $middleName,
        ?Password $password,
        ?Address $primaryAddress,
        string $avatarUrl,
        string $registrationSource,
        string $relationNumber,
        string $username
    ) {
        $this->accountId = $accountId;
        $this->created = $created;
        $this->externalId = $externalId;
        $this->bio = $bio;
        $this->consentCollection = $consentCollection;
        $this->contactNumber = $contactNumber;
        $this->dateOfBirth = $dateOfBirth;
        $this->displayName = $displayName;
        $this->email = $email;
        $this->emailVerified = $emailVerified;
        $this->familyName = $familyName;
        $this->firstContact = $firstContact;
        $this->gender = $gender;
        $this->givenName = $givenName;
        $this->isActive = $isActive;
        $this->isLite = $isLite;
        $this->isLockedOut = $isLockedOut;
        $this->isRegistered = $isRegistered;
        $this->isVerified = $isVerified;
        $this->legalAcceptances = $legalAcceptances;
        $this->middleName = $middleName;
        $this->password = $password;
        $this->primaryAddress = $primaryAddress;
        $this->avatarUrl = $avatarUrl;
        $this->registrationSource = $registrationSource;
        $this->relationNumber = $relationNumber;
        $this->username = $username;
    }

    public function getAccountId(): AccountId
    {
        return $this->accountId;
    }

    public function getCreated(): string
    {
        return $this->created;
    }

    public function getExternalId(): string
    {
        return $this->externalId;
    }

    public function getBio(): string
    {
        return $this->bio;
    }

    public function getConsentCollection(): ConsentCollection
    {
        return $this->consentCollection;
    }

    public function getContactNumber(): string
    {
        return $this->contactNumber;
    }

    public function getDateOfBirth(): ?DateOfBirth
    {
        return $this->dateOfBirth;
    }

    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getEmailVerified(): string
    {
        return $this->emailVerified;
    }

    public function getFamilyName(): string
    {
        return $this->familyName;
    }

    public function getFirstContact(): string
    {
        return $this->firstContact;
    }

    public function getGender(): string
    {
        return $this->gender;
    }

    public function getGivenName(): string
    {
        return $this->givenName;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function isLite(): bool
    {
        return $this->isLite;
    }

    public function isLockedOut(): bool
    {
        return $this->isLockedOut;
    }

    public function isRegistered(): bool
    {
        return $this->isRegistered;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function getLegalAcceptances(): LegalAcceptances
    {
        return $this->legalAcceptances;
    }

    public function getMiddleName(): string
    {
        return $this->middleName;
    }

    public function getPassword(): ?Password
    {
        return $this->password;
    }

    public function getPrimaryAddress(): ?Address
    {
        return $this->primaryAddress;
    }

    public function getAvatarUrl(): string
    {
        return $this->avatarUrl;
    }

    public function getRegistrationSource(): string
    {
        return $this->registrationSource;
    }

    public function getRelationNumber(): string
    {
        return $this->relationNumber;
    }

    public function getUsername(): string
    {
        return $this->username;
    }
}

