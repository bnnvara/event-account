<?php

declare(strict_types=1);

namespace BNNVARA\Account\Domain\ValueObject;

class Address
{
    private string $streetName;
    private string $houseNumber;
    private string $zipCode;
    private string $country;
    private string $city;
    private string $houseNumberAddition = '';

    public function __construct(
        string $streetName,
        string $houseNumber,
        string $zipCode,
        string $country,
        string $city,
        string $houseNumberAddition = ''
    ) {
        $this->streetName = $streetName;
        $this->houseNumber = $houseNumber;
        $this->zipCode = $zipCode;
        $this->country = $country;
        $this->city = $city;
        $this->houseNumberAddition = $houseNumberAddition;
    }

    public function getStreetName(): string
    {
        return $this->streetName;
    }

    public function getHouseNumber(): string
    {
        return $this->houseNumber;
    }

    public function getZipCode(): string
    {
        return $this->zipCode;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getHouseNumberAddition(): string
    {
        return $this->houseNumberAddition;
    }
}
