<?php

declare(strict_types=1);

namespace BNNVARA\Account\Domain\ValueObject;

class LegalAcceptances
{
    private string $dateAccepted;
    private bool $hasAcceptedTerms;

    public function __construct(bool $hasAcceptedTerms, string $dateAccepted = '')
    {
        $this->dateAccepted = $dateAccepted;
        $this->hasAcceptedTerms = $hasAcceptedTerms;
    }

    public function getDateAccepted(): string
    {
        return $this->dateAccepted;
    }

    public function hasAcceptedTerms(): bool
    {
        return $this->hasAcceptedTerms;
    }
}
