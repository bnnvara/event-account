<?php

declare(strict_types=1);

namespace BNNVARA\Account\Domain\ValueObject;

class Password
{
    private string $value;
    private string $type;

    public function __construct(string $value, string $type)
    {
        $this->value = $value;
        $this->type = $type;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
