# Changelog
All notable changes to this project will be documented in this file. Note: Changelogs are for HUMANS, not computers.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.9.1] - 2021-10-15
### Changed
- Naming of attributes of SynchronizeFinancialCommand and FinancialId

## [0.9] - 2021-10-04
### Changed
- Renamed SynchronizeIfundsToAkamaiCommand to SynchronizeFinancial

## [0.8] - 2021-09-22
### Added
- SynchronizeIfundsToAkamaiCommand

## [0.7] - 2021-09-20
### Added
- AccountCreatedEvent

## [0.6] - 2021-06-07
### Added
- PHP 8.0 support

## [0.5.1] - 2021-04-28
### Added
- Interface for AccountUpsertedEvents

## [0.5] - 2021-04-20
### Added
- AccountUpsertedEvent
- .gitignore
- .editorconfig
- strict types

### Changed
- Update of phpunit/phpunit
- Improved bitbucket-pipelines
- Improved run-tests.sh

## [0.4.2] - 2020-04-20
### Added
- Made objects that arent a collection Nullable.

## [0.4] - 2020-04-20
### Changed
- Password object is comprised of a Value (the password) and the Type (encryption-type)

## [0.3.2] - 2020-04-20
### Added
- AccountMergeCommand for merging lite accounts with full accounts.

## [0.3.1] - 2020-04-20
### Changed
- made the constants of Consent public

## [0.3] - 2020-04-14
### Added
- changelog

### Changed
- Removed ArrayObject from collection to make it compatible with JMS Serializer
